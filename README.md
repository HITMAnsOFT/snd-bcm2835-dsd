## Synopsis

This is an attempt on high-quality 1-bit audio output drver for Raspberry Pi,
which can be used to drive either the on-board audio output or a Class D
amplifier output stage.

## Motivation

The goal of this project is to provide the driver part for a pure digital
music player system using a Raspberry Pi 2 as the main board and a custom-
designed or ready-made high-speed switching stereo output stage, preferrably
filterless.

## Prerequisites

TODO

## Installation

TODO

## Tests

TODO

## License

This project and it's portions are distributed under Version 2 of the GNU
General Public License.