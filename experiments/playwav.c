/*
    PCM file player test code 
    Plays a .wav/.flac stereo file using 1-bit sigma-delta converter and PWM serialize mode
    Author: Arthur Simonyants
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include "bcm2835.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include <string.h>
#include <sndfile.h>

#define PINR RPI_BPLUS_GPIO_J8_32
#define PINL RPI_BPLUS_GPIO_J8_33
#define PINR_FSEL BCM2835_GPIO_FSEL_ALT0
#define PINL_FSEL BCM2835_GPIO_FSEL_ALT0
#define PWM_R 0
#define PWM_L 1
#define RANGE 32

typedef struct 
{
    int int0;
    int int1;
    int int2;
    int int3;
    int int4;
    int out0;
} SigmaDeltaState;

static unsigned int gSeed;

static inline void fastSrand(int seed) {
    gSeed = seed;
}

// Output value in range [-16384, 16383]
static inline int fastRand(void) {
    gSeed = (214013 * gSeed + 2531011);
    return ((int)((gSeed >> 16) & 0x7FFF)) - 16384;
}

static inline int clip(int value, const int c)
{
    const int max = c * SHRT_MAX * 16;
    const int min = c * SHRT_MIN * 16;
    return (value > max) ? max : ((value < min) ? min : value);
}

static inline uint32_t sigma_delta(int in, SigmaDeltaState* state)
{
    // Design by Derk Reefman and Erwin Janssen from: www.emmlabs.com/pdf/papers/DerkSigmaDelta.pdf
    /* Coefficients:
    c = {
        0.791882,
        0.304545,
        0.069930,
        0.009496,
        0.000607
    };
    f = {
        0.000496,
        0.001789
    }; */
    const int c[] = {1622, 624, 143, 19, 1};
    const int f[] = {2, 7};

    /* Initialization */
    int sum = (c[0] * state->int0
        + c[1] * state->int1
        + c[2] * state->int2
        + c[3] * state->int3
        + c[4] * state->int4) >> 11;
    uint32_t bit0 = (sum > 0);
    state->out0 = bit0 ? SHRT_MAX * 16 : SHRT_MIN * 16; // add 7 bits of precision
    state->int4 += state->int3;
    state->int3 += state->int2 - ((f[1] * state->int4) >> 12);
    state->int2 += state->int1;
    state->int1 += state->int0 - ((f[0] * state->int2) >> 12);
    state->int0 += (in - state->out0);
    return bit0;
}

static void sigma_delta_stereo(short* in, uint32_t* out, uint64_t outOffset, int frameCount, int oversample, SigmaDeltaState* state0, SigmaDeltaState* state1)
{
    assert(in); assert(out); assert(state0); assert(state1);
    for (int i = 0; i < frameCount; ++i) {
        int in0 = in[2 * i] << 3; // -6dB input
        int in1 = in[2 * i + 1] << 3;
        for (int j = 0; j < oversample; ++j) {
            uint32_t bit0 = sigma_delta(in0, state0);
            uint32_t bit1 = sigma_delta(in1, state1);
            out[2 * ((outOffset) / 32)]       |= (bit0 << (31 - (outOffset) % 32));
            out[2 * ((outOffset) / 32) + 1]   |= (bit1 << (31 - (outOffset) % 32));
            ++outOffset;
        }
    }
}

static int pwmToPdm(int frameCount, short* readbuf, int inSampleRate, uint32_t* writebuf, int outSampleRate)
{
    printf("Converting %d frames to PDM...\n", frameCount);
    const int inBlockSize = 320000;
    const int oversample = outSampleRate / inSampleRate;
    const int outBlockSize = inBlockSize * oversample;
    int inProcessed = 0;
    int outProcessed = 0;
    uint8_t lastBlock = 0;
    SigmaDeltaState sdState0 = {0, 0, 0, 0, 0, 0};
    SigmaDeltaState sdState1 = {0, 0, 0, 0, 0, 0};
    while (!lastBlock) {
        short* readPtr = readbuf + 2 * inProcessed;
        int inputFrames = 0;
        int outputFrames = 0;
        if (inProcessed + inBlockSize > frameCount) {
            inputFrames = frameCount - inProcessed;
            outputFrames = inputFrames * oversample;
            lastBlock = 1;
        } else {
            inputFrames = inBlockSize;
            outputFrames = outBlockSize;
        }
        sigma_delta_stereo(readPtr, writebuf, outProcessed, inputFrames, oversample, &sdState0, &sdState1);
        inProcessed += inputFrames;
        outProcessed += outputFrames;
        printf("%d%%\n", 100 * inProcessed / frameCount);
    }
    printf("\nConverting done.\n");
    return 0;
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("Usage: %s <input_file>\n", argv[0]);
        return 1;
    }
    const char* fileName = argv[1];

    SF_INFO info;
    /* Open the WAV file. */
    info.format = 0;
    SNDFILE* sf = sf_open(fileName, SFM_READ,&info);
    if (sf == NULL)
    {
        fprintf(stderr, "Failed to open the file %s.\n", fileName);
        return -1;
    }
    /* Print some of the info, and figure out how much data to read. */
    printf("frames=%lld\n", info.frames);
    printf("samplerate=%d\n", info.samplerate);
    printf("channels=%d\n", info.channels);
    if (info.channels != 2) {
        fprintf(stderr, "Invalid number of channels: %d, only 2 channel files are supported.\n", info.channels);
        return -1;
    }
    uint64_t num_items = info.frames * info.channels;
    printf("num_items=%lld\n",num_items);
    // preloading the whole file for the test
    printf("Loading...\n");
    short* readbuf = (short*) calloc(num_items, sizeof(short));
    if (!readbuf) {
        fprintf(stderr, "Could not allocate memory.");
        return -1;
    }
    int num = sf_read_short(sf, readbuf, num_items);
    sf_close(sf);
    printf("Read %d items\n", num);
    // Upsample and convert to PDM
    uint64_t sample_count = info.frames * 2822400ULL / info.samplerate;
    // TODO: setup DMA and double-buffering
    uint32_t* pdmbuf = (uint32_t*)malloc(sample_count / 4);
    if (!pdmbuf) {
        fprintf(stderr, "Could not allocate memory.");
        return -1;
    }
    if (pwmToPdm(info.frames, readbuf, info.samplerate, pdmbuf, 2822400)) {
        return -1;
    }
    free(readbuf);
    if (!bcm2835_init()) {
        return 1;
    }
    // Set the output pin to Alt Fun 5, to allow PWM channel 0 to be output there
    bcm2835_gpio_fsel(PINR, PINR_FSEL);
    bcm2835_gpio_fsel(PINL, PINL_FSEL);
    // Set DSD clock at 500MHz / 177 ~ 2824858 Hz, within 0.1% of standard 2822400 Hz
    // This is the closest you can get without using fractional divider, which adds audible noise to the output
    bcm2835_pwm_set_clock(177, 0, 0, BCM2835_PWM_CLOCK_SOURCE_PLLD); 
    // Serialize mode, enable FIFO, enable PWM
    bcm2835_pwm_set_mode(PWM_R, 0, 1, 1, 1);
    bcm2835_pwm_set_mode(PWM_L, 0, 1, 1, 1);
    // Set range to the full 32 bit word
    bcm2835_pwm_set_range(PWM_R, RANGE);
    bcm2835_pwm_set_range(PWM_L, RANGE);

    printf("Playing...\n");
    for (int i = 0; i < sample_count / 16; ++i) {

        while (!bcm2835_pwm_has_space()) {
            // TODO: don't waste CPU, might need DMA for that. 
        }
        bcm2835_pwm_push_data(pdmbuf[i]);
    }
    usleep(100); // 16x32 fifo holds around 90us data
    printf("Done.\n");
    free(pdmbuf);
    bcm2835_close();
    return 0;
}
