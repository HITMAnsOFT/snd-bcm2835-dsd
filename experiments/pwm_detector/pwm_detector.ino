#pragma GCC optimize ("-O3")
/*
    ATTiny85 sketch for detecting PWM activity and waking up/unmuting the Class
    D output stage and putting it to mute/sleep as soon as the signal
    disappears. Designed to work with the new modulation scheme of the
    Raspberry Pi onboard sound driver. Tested with 8MHz internal OSC, may or
    may not work with 1MHz.

    Author: Arthur Simonyants

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

constexpr int inPin = 2;  // PB2, hardware pin 7
constexpr int outPin = 1; // PB1, hardware pin 6
constexpr int interrupt = 0;

volatile bool seenPulse = false;

constexpr uint8_t timeout = F_CPU / 250000; // 4uS inactivity timeout


static inline void writeOutput(const bool value)
{
  PORTB = value ? _BV(outPin) | PORTB : ~_BV(outPin) & PORTB;  
}

static inline void startTimer()
{
  TCCR0B = _BV(CS00); // Timer runs at clock frequency
}

static inline void stopTimer()
{
  TCCR0B = 0;
}

static inline void resetTimer()
{
  TCNT0 = 0;
  TIFR |= _BV(OCF0A); //clear interrupt flag
}

static void setupPins()
{
  pinMode(inPin, INPUT);
  writeOutput(false);
  pinMode(outPin, OUTPUT);  
}

static void setupTimer()
{
  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0 = 0;

  TCCR0A = _BV(WGM01); // CTC mode, no outputs
  OCR0A = timeout; // set timeout value
  TIFR |= _BV(OCF0A); //clear interrupt flag
  TIMSK = _BV(OCIE0A); // enable interrupt on timeout
}

static void setupInterrupt()
{
  MCUCR |= 3; // INT0 on rising edge
  GIMSK |= 0x40; // enable INT0
}

void setup()
{
  noInterrupts();
  setupPins();
  setupTimer();
  setupInterrupt();
  interrupts();
}

void loop()
{
  // Nothing to be done here, whole code is interrupt-based
}

ISR(INT0_vect)
{
  resetTimer();
  if (seenPulse) {
    writeOutput(true);
  } else {
    startTimer();
    seenPulse = true;
  }
}

ISR(TIMER0_COMPA_vect)
{
  seenPulse = false;
  stopTimer();
  writeOutput(false);
}

