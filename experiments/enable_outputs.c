/*
    Small utility to enable PWM outputs on GPIO pins
    Author: Arthur Simonyants
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include "bcm2835.h"

#define PINR RPI_BPLUS_GPIO_J8_32
#define PINL RPI_BPLUS_GPIO_J8_33
#define PINR_FSEL BCM2835_GPIO_FSEL_ALT0
#define PINL_FSEL BCM2835_GPIO_FSEL_ALT0


int main(int argc, char **argv)
{
    if (!bcm2835_init()) {
        return 1;
    }
    // Set the output pin to Alt Fun 5, to allow PWM channel 0 to be output there
    bcm2835_gpio_fsel(PINR, PINR_FSEL);
    bcm2835_gpio_fsel(PINL, PINL_FSEL);
    bcm2835_close();
    return 0;
}
